$(document).ready(function () {
	
	$.get("dashboard/xhrGetAll", function(o){
		var data = JSON.parse(o);
		for (var i = 0; i < data.length; i++) {
			$('#listing').append("<div>"+data[i].text+"<a href='#' rel='"+data[i].id+"' class='del'> X </a></div>");
		};


		$(document).on('click', '.del', function(o){

			var id = $(this).attr('rel');
			var delItem = $(this);

			$.post("dashboard/xhrDelete", {id: id}, function(o){
		            delItem.parent().remove();

			});

			return false;
		});

	});

	$('#sub').click(function(){
		var url = "dashboard/xhrInsert";
		var name  = $('#name').val();

		$.ajax({
	        type: 'POST',
	        url: url,
	        data: {
	        	'name': name
	        },
	        success: function (data) {
	        	var data = JSON.parse(data);
	            $('#listing').append("<div>"+name+"<a href='#' rel='"+data.id+"' class='del'> X </a></div>");
	        },
	        error: function(jqXHR,error, errorThrown) {  
	            if(jqXHR.status&&jqXHR.status==400){
	                alert(jqXHR.responseText); 
	            }else{
	                alert("Something went wrong");
	            }
	        }
    	});

		return false;
	});

});