<div class='row'>

<div class='large-6 columns'>
<form method="post" action="<?php echo BASE_PATH?>user/saveEdit/<?php echo $this->singleUserData['id'];?>">
	<fieldset>
	<legend>Edit User</legend>

	  <div class="row">
	    <div class="large-6">     
	        <input type="text" id="right-label" placeholder="User Name" name="login" value="<?php echo $this->singleUserData['login'];?>">       
	    </div>
	  </div>

	  <div class="row">
	    <div class="large-6">     
	        <input type="password" id="right-label" placeholder="Password" name="password">      
	    </div>
	  </div>

	  <div class="row">
	    <div class="large-4">     
	       <select name="role">
	       	<option value="default" <?php if($this->singleUserData['role']=='default') echo 'selected'?>>Default</option>
	       	<option value="admin" <?php if($this->singleUserData['role']=='admin') echo 'selected'?>>Admin</option>
	       	<option value="owner" <?php if($this->singleUserData['role']=='owner') echo 'selected'?>>Owner</option>
	       </select>    
	    </div>
	  </div>

	  <div class="row">
	    <div class="large-6">
	       <input type="submit" id="right-label"class="button small" value="Edit">    
	    </div>
	  </div>

	</fieldset>
</form>
</div>

<div class='large-6 columns'>
&nbsp;
</div>
</div>