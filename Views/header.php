<html>
<head>
<title>SimplyMVC</title>
<link rel="stylesheet" type="text/css" href="<?php echo BASE_PATH; ?>Public/css/foundation.min.css">
<link rel="stylesheet" type="text/css" href="<?php echo BASE_PATH; ?>Public/css/marketing.css">
<script type="text/javascript" src="<?php echo BASE_PATH; ?>Public/js/jquery.js"></script>
<script type="text/javascript" src="<?php echo BASE_PATH; ?>Public/js/default.js"></script>
</head>
<body>
<div id="header">

<nav class="top-bar hide-for-small">
  <ul class="title-area">
    <li class="name">
      <h1><a href="<?php echo BASE_PATH;?>index">MVC Demo</a></h1>
    </li>
  </ul>

  <section class="top-bar-section">
    <ul class="right">            
      <li>
        <?php if(Session::get("isLoggedIn") == true): ?> 
          <?php if(Session::get("role") == 'owner'): ?> 
          <li>
            <a href="<?php echo BASE_PATH;?>user" class="">Users</a>
          </li>
          <?php endif ?>
    			 <a href="<?php echo BASE_PATH;?>dashboard/logout" class="small button">Logout</a>
    		<?php else: ?>
          <li>
            <a href="<?php echo BASE_PATH;?>index" class="">Home</a>
          </li>
          <li>
            <a href="<?php echo BASE_PATH;?>index" class="">Help</a>
          </li>
    			<a href='<?php echo BASE_PATH;?>login' class="small button">Login</a>
    		<?php endif ?>
      </li>
    </ul>
  </section>
</nav>


</div>
<div id="content">