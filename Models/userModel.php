<?php

class UserModel extends BaseModel
{
	
	function __construct()
	{
		parent::__construct();
	}

	function fetchData()
	{
		$stmt = $this->db->prepare("SELECT id, login, pw, role FROM tbl_user");
		$stmt->execute();
		$data = $stmt->fetchAll();

		return $data;
	}

	function fetchSingleData($id)
	{
		$stmt = $this->db->prepare("SELECT id, login, pw, role FROM tbl_user WHERE id=:id");
		$stmt->execute(array(
		':id' => $id));
		$data = $stmt->fetch();

		return $data;
	}


	function addData()
	{
		$stmt = $this->db->prepare("INSERT INTO tbl_user(`login`,`pw`,`role`) VALUES(:login, md5(:pw), :role)");
		$stmt->execute(array(
		':login' => $_POST['login'],
		':pw' => $_POST['password'],
		':role' => $_POST['role']
		 ));
	}

	function saveData($id)
	{
		$stmt = $this->db->prepare("UPDATE tbl_user SET login = :login, pw = md5(:pw),  role = :role WHERE id = :id");
		$stmt->execute(array(
		':id' => $id,
		':login' => $_POST['login'],
		':pw' => $_POST['password'],
		':role' => $_POST['role']
		 ));
	}

	function deleteData($id)
	{
		$stmt = $this->db->prepare("DELETE FROM tbl_user WHERE id = :id");
		$stmt->execute(array(':id' => $id));
	}
}
?>