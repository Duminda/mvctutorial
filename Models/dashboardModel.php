<?php

class DashboardModel extends BaseModel
{
	
	function __construct()
	{
		parent::__construct();
	}

	function xhrInsert()
	{
		$stmt = $this->db->prepare('INSERT INTO tbl_xhr(text) VALUES(:text)');
		$stmt->execute(array(":text" => $_POST['name']));

		echo json_encode(array('name'=> $_POST['name'], 'id' => $this->db->lastInsertId()));

	}

	function xhrGetAll()
	{
		$stmt = $this->db->prepare('SELECT * FROM tbl_xhr');
		$stmt->setFetchMode(PDO::FETCH_ASSOC);
		$stmt->execute();
		$data = $stmt->fetchAll(); 
		
		echo json_encode($data);
	}

	function xhrDelete()
	{
		$stmt = $this->db->prepare('DELETE FROM  tbl_xhr WHERE id= :id');
		$stmt->execute(array('id' => $_POST['id']));
	}
}
?>