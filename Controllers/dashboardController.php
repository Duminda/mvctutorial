<?php

class DashboardController extends BaseController
{
	
	function __construct()
	{
		parent::__construct();
		Session::init();

		if(Session::get("isLoggedIn") == false)
		{
			echo Session::get("isLoggedIn");
			Session::destroy();
			header("location: login");
			exit;
		}
	}

	function index()
	{
		$this->view->loadView('dashboard/index');
	}

	function logout()
	{
		Session::destroy();
		header("location: ../login");
		exit;
	}

	function xhrInsert()
	{
		$this->model->xhrInsert();
	}

	function xhrGetAll()
	{
		$this->model->xhrGetAll();
	}

	function xhrDelete()
	{
		$this->model->xhrDelete();
	}
}

?>