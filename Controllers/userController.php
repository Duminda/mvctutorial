<?php

class UserController extends BaseController
{
	
	function __construct()
	{
		parent::__construct();
		Session::init();

		if(Session::get("role") == 'default')
		{
			Session::destroy();
			header("location: login");
			exit;
		}
	}

	function index()
	{
		$this->view->userData = $this->model->fetchData();
		$this->view->loadView('user/index');
	}

	function create()
	{	
		$this->model->addData();
		header("location: ../user");
	}

	function edit($id)
	{
		$this->view->singleUserData = $this->model->fetchSingleData($id);
		$this->view->loadView('user/edit');
	}

	function saveEdit($id)
	{
		$this->model->saveData($id);
		header("location: ../../user");
	}

	function delete($id)
	{
		$this->model->deleteData($id);
		header("location: ../../user");
	}

}

?>