<?php

class IndexController extends BaseController
{
	
	function __construct()
	{
		parent::__construct();
	}

	function index()
	{
		$this->view->loadView('index/index');
	}

}

?>
